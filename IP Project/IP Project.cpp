// OpenCVApplication.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "common.h"
#include <cmath>
#include <queue>
#include <random>
#include <math.h>

//Lab stuff.
bool isInside(Mat src, int i, int j) {
	if (i < src.rows && j < src.cols && i>=0 && j>=0) {
		return true;
	}
	return false;
}

//Project Stuff.

//Configs and stuff.
#define VAR 15			//Variance for gaussian noise.
#define MAXI 255		//Maximum possible value for a pixel - greyscale.
int r = 2;							//Window "radius".
double a = 0.1;						//Adaptive factor.
double e = 2.5 * VAR;				//No idea.

//Let there be noise.
void makeImageNoisy(Mat_<uchar> *source, int var=VAR) {
	Mat_<float> noise(source->rows, source->cols);
	randn(noise, 0, var);
	*source += noise;
}

//Compute mean squared error.
double MSE(Mat_<uchar> source, Mat_<uchar> image) {
	//Validate input.
	if (source.rows != image.rows || source.cols != image.cols)
		return -1.0;
	double mse = 0;
	for (int i = 0; i < source.rows; i++) {
		for (int j = 0; j < source.cols; j++) {
			mse += pow((source(i, j) - image(i, j)), 2);
		}
	}
	mse /= (source.rows * source.cols);
	return mse;
}

//Compute peak-to-peak signal to noise ratio, expressed in decibels (dB). Higher is better.
double PSNR(Mat_<uchar> source, Mat_<uchar> image) {
	//Validate input.
	if (source.rows != image.rows || source.cols != image.cols)
		return -1.0;
	double psnr = 20.0 * log10(MAXI) - 10.0 * log10(MSE(source, image));
	return psnr;
}

////Standard algorithm.

//Compute Local Mean.
double localMean(Mat_<uchar> image, int i, int j) {
	double ux = 0.0;
	double windowSize = 2.0 * r + 1.0; 
	for (int p = i - r; p <= i + r; p++) {
		for (int q = j - r; q <= j + r; q++) {
			if (isInside(image, p, q))
				ux += image(p, q);
		}
	}
	ux /= pow(windowSize, 2);
	return ux;
}

//Compute Local Variance.
double localVariance(Mat_<uchar> image, int i, int j, int ux) {
	double sx = 0.0;
	double windowSize = 2.0 * r + 1.0;
	for (int p = i - r; p <= i + r; p++) {
		for (int q = j - r; q <= j + r; q++) {
			if (isInside(image, p, q)) {
				sx += pow((image(p, q) - ux), 2);
			}
		}
	}
	sx /= pow(windowSize, 2);
	sx -= pow(VAR, 2);
	sx = sqrtf(abs(sx)); 
	return sx;
}

////Weighted algorithm:
#define NUM(image, i, j, p, q)  (1 + a * (max(pow(e, 2), pow(image(i, j) - image(p, q), 2))))

//Compute K.
inline double K(Mat_<uchar> image, int i, int j) {
	double K = 0.0;
	for (int p = i - r; p <= i + r; p++)
		for (int q = j - r; q <= j + r; q++)
			if (isInside(image, p, q))
				K += 1.0 / NUM(image, i, j, p, q);
	K = 1.0 / K;
	return K;
}

//Compute Weight.
double weight(Mat_<uchar> image, int i, int j, int p, int q, double K, double a) {
	double e = 2.5 * VAR;
	double w = K/ NUM(image, i, j, p, q);;
	return w;
}

//Compute Weighted Local Variance.
double weightedLocalVariance(Mat_<uchar> image, int i, int j, int ux) {
	double sx = 0.0;					//Local Weighted Variance.
	double windowSize = 2.0 * r + 1.0;	//Window Size.
	double k = 0.0;						//Constant smth.

	//Compute K.
	k = K(image, i, j);

	//Compute Variance.
	for (int p = i - r; p <= i + r; p++) 
		for (int q = j - r; q <= j + r; q++) 
			if (isInside(image, p, q)) 
				sx += weight(image, i, j, p, q, k, a) * pow((image(p, q) - ux), 2);

	sx = sqrtf(abs(sx));
	return sx;
}

//Compute Weighted Local Mean.
double weightedLocalMean(Mat_<uchar> image, int i, int j) {
	double ux = 0.0;					//Local Weighted Mean.
	double windowSize = 2.0 * r + 1.0;	//Window size.
	double k = 0.0;						//Constant smth.

	//Compute K.
	k = K(image, i, j);

	//Compute Mean.
	for (int p = i - r; p <= i + r; p++) 
		for (int q = j - r; q <= j + r; q++) 
			if (isInside(image, p, q))
				ux += weight(image, i, j, p, q, k, a) * image(p, q) ;

	return ux;
}

//Compute and apply Wiener filter on matrix.
void wienerFilter(Mat_<uchar> *source) {
	Mat_<uchar> image = source->clone();
	double ux = 0.0;	//Signal(x) mean.
	double sx = 0.0;	//Signal(x) variance.
	double windowSize = 2.0 * r + 1.0;
	for (int i = 0; i < source->rows; i++) {
		for (int j = 0; j < source->cols; j++) {
			ux = 0.0;
			sx = 0.0;

			//Compute local signal mean.
			ux = localMean(image, i, j);

			//Compute local signal variance.
			sx = localVariance(image, i, j, ux);

			//Compute denoised pixel value with Wiener filter.
			uchar original = image(i, j);
			(*source)(i, j) = (pow(sx, 2) / (pow(sx, 2) + pow(VAR, 2))) * (image(i, j) - ux) + ux;
		}
	}
}

//Compute and apply Wiener filter on matrix.
void adaptiveWienerFilter(Mat_<uchar> *source) {
	Mat_<uchar> image = source->clone();
	double ux = 0.0;	//Signal(x) mean.
	double sx = 0.0;	//Signal(x) variance.
	double windowSize = 2.0 * r + 1.0;
	for (int i = 0; i < source->rows; i++) {
		for (int j = 0; j < source->cols; j++) {
			ux = 0.0;
			sx = 0.0;

			//Compute local signal mean.
			ux = weightedLocalMean(image, i, j);

			//Compute local signal variance.
			sx = weightedLocalVariance(image, i, j, ux);

			//Compute denoised pixel value with Wiener filter.
			uchar original = image(i, j);
			(*source)(i, j) = (pow(sx, 2) / (pow(sx, 2) + pow(VAR, 2))) * (image(i, j) - ux) + ux;
		}
	}
}

//Main... pretty self explanatory.
int main(){
	char fname[MAX_PATH];
	while (openFileDlg(fname)) {
		double mse = 0;
		double psnr = 0;
		Mat_<uchar> source = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		Mat_<uchar> imageNoisy = source.clone();
		
		//Make image noisy.
		makeImageNoisy(&imageNoisy);
		
		//Clean image with standard filter.
		Mat_<uchar> imageCleaned = imageNoisy.clone();
		wienerFilter(&imageCleaned);

		//Clean image with adaptive filter.
		Mat_<uchar> imageCleanedAdaptive = imageNoisy.clone();
		adaptiveWienerFilter(&imageCleanedAdaptive);

		//Compute MSE.
		if ((mse = MSE(source, imageNoisy)) < 0.0) {
			printf("MSE: Images' sizes don't match.\n");
			return -1;
		}
		printf("MSE on noised image: %f.\n", mse);

		//Compute PSNR.
		if ((psnr = PSNR(source, imageNoisy)) < 0.0) {
			printf("PSNR: Images' sizes don't match.\n");
			return -1;
		}
		printf("PSNR on noised image: %f dB.\n", psnr);


		//Compute MSE on standard cleaned image.
		if ((mse = MSE(source, imageCleaned)) < 0.0) {
			printf("MSE: Images' sizes don't match.\n");
			return -1;
		}
		printf("MSE on standard cleaned image: %f.\n", mse);

		//Compute PSNR on standard cleaned image.
		if ((psnr = PSNR(source, imageCleaned)) < 0.0) {
			printf("PSNR: Images' sizes don't match.\n");
			return -1;
		}
		printf("PSNR on standard cleaned image: %f dB.\n", psnr);
		

		//Compute MSE on adaptive cleaed image.
		if ((mse = MSE(source, imageCleanedAdaptive)) < 0.0) {
			printf("MSE: Images' sizes don't match.\n");
			return -1;
		}
		printf("MSE on adaptive cleaned image: %f.\n", mse);

		//Compute PSNR on adaptive image.
		if ((psnr = PSNR(source, imageCleanedAdaptive)) < 0.0) {
			printf("PSNR: Images' sizes don't match.\n");
			return -1;
		}
		printf("PSNR on adaptive cleaned image: %f dB.\n", psnr);

		//Display images.
		imshow("Source Image", source);
		imshow("Noisy Image", imageNoisy);
		imshow("Standard Cleaned Image ", imageCleaned);
		imshow("Adaptive Cleaned Emage", imageCleanedAdaptive);
		waitKey(0);
	}

	return 0;
}